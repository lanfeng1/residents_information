from django.db import models

# Create your models here.
class residents_information(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    age = models.CharField(max_length=20, blank=True, null=True)
    idcard = models.CharField(max_length=20, blank=True, null=True)
    car = models.CharField(max_length=128, blank=True, null=True)
    gender = models.CharField(max_length=128, blank=True, null=True)
    crowd = models.CharField(max_length=128, blank=True, null=True)
    special_populations = models.CharField(max_length=128, blank=True, null=True)
    site = models.CharField(max_length=128, blank=True, null=True)
    face = models.CharField(max_length=128, blank=True, null=True)
    relation = models.CharField(max_length=128, blank=True, null=True)
    units = models.TextField(max_length=128, blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    class Meta:
        verbose_name = '常驻居民信息'
        verbose_name_plural = verbose_name


class mobile_residents(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    age = models.CharField(max_length=20, blank=True, null=True)
    idcard = models.CharField(max_length=20, blank=True, null=True)
    date = models.CharField(max_length=20, blank=True, null=True)
    gender = models.CharField(max_length=128, blank=True, null=True)
    lease = models.CharField(max_length=128, blank=True, null=True)
    site = models.CharField(max_length=128, blank=True, null=True)
    face = models.CharField(max_length=128, blank=True, null=True)
    home = models.CharField(max_length=128, blank=True, null=True)
    units = models.TextField(max_length=128, blank=True, null=True)
    username = models.CharField(max_length=128, blank=True, null=True)
    userphone = models.CharField(max_length=128, blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    class Meta:
        verbose_name = '流动居民信息'
        verbose_name_plural = verbose_name

class volunteers(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    age = models.CharField(max_length=20, blank=True, null=True)
    specialty = models.CharField(max_length=128, blank=True, null=True)
    volunteer_time = models.CharField(max_length=128, blank=True, null=True)
    regional = models.CharField(max_length=128, blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    class Meta:
        verbose_name = '志愿者信息'
        verbose_name_plural = verbose_name